/* =======================================================================
Visual Website Optimizer custom js
======================================================================= */
// HEADER PARTICLES EFFECT

//Map javascript///
$description = $(".description");
$('.enabled').hover(function () {

  $(this).attr("class", "enabled heyo");
  $description.addClass('active');
  $description.html($(this).attr('id'));
}, function () {
  $description.removeClass('active');
});

$(document).on('mousemove', function (e) {

  $description.css({
    left: e.pageX,
    top: e.pageY - 70
  });

});
///smooth scroll

$(document).ready(function () {
  // Add smooth scrolling to all links
  $(".jump-tabs a,.scrollDown a").on('click', function (event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function () {

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});


//loader//
$(window).load(function () {
  $(".loader").fadeOut("slow");
});


//youtube video
jQuery(function () {
  jQuery("a.play").YouTubePopUp();
});

//web animation
new WOW().init();


//particles
particlesJS("particles-js", {
  "particles": {
    "number": {
      "value": 30,
      "density": {
        "enable": false,
        "value_area": 2000
      }
    },
    "color": {
      "value": "#e7e9f2"
    },
    "shape": {
      "type": "polygon",
      "stroke": {
        "width": 0,
        "color": "#ffffff"
      },
      "polygon": {
        "nb_sides": 5
      },
      "image": {
        "src": "img/github.svg",
        "width": 300,
        "height": 300
      }
    },
    "opacity": {
      "value": 0.5,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 1,
        "opacity_min": 0.1,
        "sync": false
      }
    },
    "size": {
      "value": 0,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 21.926084732136317,
        "size_min": 0,
        "sync": false
      }
    },
    "line_linked": {
      "enable": true,
      "distance": 200,
      "color": "#ffffff",
      "opacity": 0.2,
      "width": 1.2
    },
    "move": {
      "enable": true,
      "speed": 5,
      "direction": "none",
      "random": false,
      "straight": false,
      "out_mode": "bounce",
      "bounce": false,
      "attract": {
        "enable": false,
        "rotateX": 481.0236182596568,
        "rotateY": 320.6824121731046
      }
    }
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": false,
        "mode": "bubble"
      },
      "onclick": {
        "enable": false,
        "mode": "bubble"
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 300,
        "line_linked": {
          "opacity": 0.09048805968921457
        }
      },
      "bubble": {
        "distance": 0,
        "size": 0,
        "duration": 0,
        "opacity": 0,
        "speed": 3
      },
      "repulse": {
        "distance": 0.5,
        "duration": 0.4
      },
      "push": {
        "particles_nb": 4
      },
      "remove": {
        "particles_nb": 2
      }
    }
  },
  "retina_detect": true
});
var count_particles, stats, update;
stats = new Stats;
stats.setMode(0);
stats.domElement.style.position = 'absolute';
stats.domElement.style.left = '0px';
stats.domElement.style.top = '0px';
document.body.appendChild(stats.domElement);
count_particles = document.querySelector('.js-count-particles');
update = function () {
  stats.begin();
  stats.end();
  if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
    count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
  }
  requestAnimationFrame(update);
};
requestAnimationFrame(update);;


